%!TEX root = paper.tex

\section{Related Work}
\label{sec:related}

MLlib, Mahout \cite{mahout}, and MADLib \cite{madlib} are machine learning \emph{libraries} 
on top of distributed computing platforms and relational engines.
All of them provide many standard machine learning models such as LDA and SVM.
However, when a domain user, say, a machine learning researcher,
is devising and testing her customized models with her big data,
those libraries cannot help.

MATLAB and R have been the most popular systems for implementing inference algorithms. 
For example, R has a package SparkR that serves as an interface to Spark utilities including MLlib.
These systems largely targets algorithm designers, rather than end users
such as data scientists or statisticians.
Another disadvantage of these systems is that they can hardly scale up
when faced with increasing amount of data
because they mostly work on a single machine and thus cannot easily scale out.
It is possible to first transform a large dataset into a smaller one
using MapReduce or Spark and then load the data to MATLAB or R to perform inference %on the smaller ones in
in some scenarios. But this approach involves multiple systems, which is hard to
develop code for and inefficient because of the data movement.
SystemML \cite{systemml} provides a similar interface for implementing inference
algorithms and transforms them to Hadoop MapReduce. It can scale out to large
clusters but coding on such a system still requires extensive knowledge in
statistical inference for end-user.

In contrast to the systems above, MLBase \cite{mlbase}
targets end-users who are not machine learning experts.
It exposes a declarative language interface and
provides a cost-based optimizer that selects the best algorithm and
parameters. It also provides MLI \cite{mli}, a set of APIs for customizing algorithms on
Spark. However, MLBase mainly supports frequentist approach, such as SVM,
and logistic regression. It does not deal with general Bayesian inference.
PyMC~\cite{patil2010pymc} supports Bayesian statistical model in Python.  It can handle custom
graphical model, however MCMC is the only inference algorithm provided, hence
the name of the system.  Moreover, PyMC does not support parallel computation.

There are a number of probabilistic programming frameworks other than
Infer.NET \cite{InferNET14}.  For example, Church \cite{GMR+08} is a
probabilistic programming language based on the functional programming
language Scheme.  Church programs are interpreted rather than compiled.
Random draws from a basic distribution and queries about the execution trace
are two additional type of expressions. A Church expression defines a
generative model. Queries of a Church expression can be conditioned on any
valid Church expressions. Nested queries and recursive functions are also
supported by Church. Church supports stochastic-memoizer which can be used to
express nonparametric models.  Despite the expressive power of Church, it
cannot scale for large dataset and models.  Figaro is a probabilistic
programming language implemented as a library in Scala \cite{Figaro}.  It is
similar to Infer.NET in the way of defining models and performing inferences
but put more emphasis to object-orientation. Models are defined by composing
instances of Model classes defined in the Figaro library.  Infer.NET is a
probabilistic programming framework in C\# for Bayesian Inference. A rich set
of variables are available for model definition. Models are converted to a
factor graph on which efficient built-in inference algorithms can be applied.
Infer.NET is the best optimized probabilistic programming frameworks so far.
Unfortunately, all existing probabilistic programming frameworks including
Infer.NET cannot scale out on to a distributed platform.

The work that comes close to distributed bayesian inference is by
Alex Beutel et al.~\cite{BeutelTKFPX14,beutel2014elastic}. They target
specific models such as Bayesian collaborative filtering which is amenable to
pipeline parallelism. The proposed framework partitions tensor or matrix 
so that each partition gets processed on a distributed machine. Then
on each local machine, infer.NET is used for Bayesian inferece. 
What gets distributed is the input matrix and not the Bayesian model. Hence
strictly speaking, this is not a distributed probabilistic programming
framework. 

To the best of our knowledge, InferSpark is the only framework that can
efficiently carry out large-scale Bayesian inference through probabilistic
programming on a distributed in-memory computing platform. It targets
end-users who are not expert in Bayesian inference or distributed computing.
The inference implementation can scale out to a large cluster and scale up to
large datasets.

%
%Our proposed probabilistic programming language is also an extension to the existing language Scala, but differs from the way that Infer .NET and Figaro define models that we add additional language constructs to Scala rather than implement a library. It typically enables a cleaner syntax.







%\item Other inference algorithms for Bayesian Networks.



\cut{%%%%%%%%%%%%%%
The Apache Hadoop library \cite{hadoop} is a distributed data storage and processing framework. Its distributed file system HDFS support the storage of large data. MapReduce is the programming model for distributed parallel processing of the large data. It is composed of two key operations map and reduce. Map operation transforms and filters the data on different nodes in parallel while reduce operations combines the results from map operation to produce results grouped by keys. In our proposal of PP, data can be distribted on a HDFS.

Spark is another distributed data processing framework that provides MapReduce operations. It differs from Hadoop in that it does not write the intermediate results to temporary storage. Instead, it caches the results in memory as resilient distributed dataset \cite{Zaharia:2012:RDD:2228298.2228301}. It can greatly speed up the processing.

The Spark built-in machine learning library MLlib \cite{mllib} provides a variety of standard statistical inference algorithms including classification, regression, clustering, collaborative filtering and dimensionality reduction. The algorithms leverage the Spark infrastructure so that large-scale data can be processed efficiently. The algorithms are applicable when the standard models fit the data well. Users have to directly develop their own algorithms using Spark or GraphX API when a customized model has to be used. Our integration of PP with Spark can greatly reduce the amount of work to implement a new model.

GraphX \cite{Xin:2013:GRD:2484425.2484427} is Spark's built-in graph parallel computation API. User can view graphs as normal RDDs of vertices and edges and perform normal map reduce operations on them or perform graph operations like compute subgraph, reversing edges, join vertices. The Pregel operator in GraphX is used to express iterative algorithms. In each step, vertices aggregates messages along the inbound edges from previous step, compute its new value and sends messages along outbound edges in parallel. It terminates when no message is sent during a step. Our PP will be built on GraphX since it is natural to represent the factor graph in GraphX and leverage the parallel graph computing to implement message-passing style inference algorithms.

\KZ{Reduce the discussion of the following PP languages a bit. Focus on
their big-data handling capabilities, or the lack of which.}

Many recent probabilistic programming languages are implemented by extending existing conventional programming languages such as Scheme, C\#, Scala and etc.
We examine three probabilistic programming languages Church \cite{GMR+08}, Infer .NET \cite{InferNET14} and Figaro \cite{pfeffer2009figaro}.

Church is a probabilistic programming language based on the functional programming language Scheme. Random draws from a basic distribution and queries about the execution trace are two additional type of expressions. A church expression defines a generative model. Queries of a church expression can be conditioned on any valid church expressions. Nested queries and recursive functions are also supported by Church. Church supports stochastic-memoizer which can be used to express nonparametric models. Thus a wide range of probabilistic models can be concisely expressed in Church. Inference in Church is implemented by rejection sampling and Metropolis-Hastings sampling algorithms. Despite the expressive power of Church, it cannot scale up to large dataset and models.

Our approach differs from Church in that our program is compiled rather than interpreted. Compiled program is usually more efficient than interpreted ones. We can also leverage the distributed parallel processing to scale up to large dataset and models.

Infer .NET is a probabilistic programming framework in C\# for Bayesian Inference. A rich set of variables are available for model definition. Models are converted to a factor graph on which efficient built-in inference algorithms can be applied. The algorithms include expectation propagation, variational message passing, block Gibbs sampling. Infer .NET addresses the scalability by shared variables, as discussed in Section \ref{sec:intro}.
%% ?? not sure about how the shared variable is implemented

Figaro is a probabilistic programming language implemented as a library in Scala. It is similar to Infer .NET in the way of defining models and performing inferences but put more emphasis to object-orientation. Models are defined by composing instances of Model classes defined in the Figaro library.

Our proposed probabilistic programming language is also an extension to the existing language Scala, but differs from the way that Infer .NET and Figaro define models that we add additional language constructs to Scala rather than implement a library. It typically enables a cleaner syntax.

}%%%%%%%%%%%%%%%%
